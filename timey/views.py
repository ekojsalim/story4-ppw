import datetime
from django.shortcuts import render

# Create your views here.


def time(request, offset=0):
    t = datetime.datetime.now()
    t = t + datetime.timedelta(hours=offset+7)
    return render(request, "timey/time.html", {"time": t, "oft": offset})
