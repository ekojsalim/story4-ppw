from django.urls import path

from .views import time

urlpatterns = [
    path('', time, name='default-time'),
    path('<int:offset>', time, name='time'),
]
