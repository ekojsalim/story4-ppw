from django.apps import AppConfig


class TimeyConfig(AppConfig):
    name = 'timey'
