from django.forms import ModelForm, Form, ChoiceField
from .models import Course


class CourseForm(ModelForm):
    class Meta:
        model = Course
        fields = '__all__'


class DeleteCourseForm(Form):
    def __init__(self, matkuls, *args, **kwargs):
        super(DeleteCourseForm, self).__init__(*args, **kwargs)
        self.fields["courses"].choices = matkuls

    courses = ChoiceField(choices=(), required=True, label="Course Name")
