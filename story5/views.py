from django.shortcuts import render, redirect
from .forms import CourseForm, DeleteCourseForm
from .models import Course, Task
import datetime


def add_course(req):
    courses = [(course.name, course.name) for course in Course.objects.all()]
    if req.method == 'POST':
        form = CourseForm(req.POST)
        if form.is_valid():
            new_course = form.save()
            return redirect('all-course')
    else:
        form = CourseForm()
    delete_form = DeleteCourseForm(courses)
    return render(req, 'story5/addcourse.html', {'form': form, 'delete_form': delete_form})


def delete_course(req):
    courses = [(course.name, course.name) for course in Course.objects.all()]
    if req.method == 'POST':
        delete_form = DeleteCourseForm(courses, req.POST)
        if delete_form.is_valid():
            Course.objects.get(pk=req.POST.get('courses')).delete()
            return redirect('all-course')
    else:
        delete_form = DeleteCourseForm(courses)
    form = CourseForm()
    return render(req, 'story5/addcourse.html', {'form': form, 'delete_form': delete_form})


def all_course(req):
    all_course = Course.objects.all()
    return render(req, 'story5/allcourse.html', {'courses': all_course})


def view_course(req, name):
    course = Course.objects.get(pk=name)
    tasks = course.tasks.all()
    currtime = datetime.datetime.now()
    past_tasks = [t for t in tasks if t.deadline < currtime.date()]
    close_tasks = [t for t in tasks if (
        t.deadline - currtime.date()).days <= 7 and t not in past_tasks]
    tasks = [
        t for t in tasks if t not in past_tasks and t not in close_tasks]
    return render(req, 'story5/course.html', {'course': course, 'tasks': tasks, 'past_tasks': past_tasks, 'close_tasks': close_tasks, 'tasks': tasks})
