from django.db import models


class Course(models.Model):
    name = models.CharField(max_length=40, primary_key=True)
    lecturer = models.CharField(max_length=60)
    credit = models.PositiveSmallIntegerField()
    description = models.TextField()
    term = models.CharField(max_length=50)
    room = models.CharField(max_length=50)


class Task(models.Model):
    name = models.CharField(max_length=60)
    description = models.TextField()
    course = models.ForeignKey(
        Course, on_delete=models.CASCADE, related_name='tasks')
    deadline = models.DateField()

    class Meta:
        ordering = ['-deadline']
