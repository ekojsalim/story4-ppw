from django.urls import path, include
from .views import add_course, all_course, view_course, delete_course

urlpatterns = [
    path('add', add_course, name='add-course'),
    path('delete', delete_course, name='delete-course'),
    path('', all_course, name='all-course'),
    path('course/<str:name>', view_course, name='view-course')
]
