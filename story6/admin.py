from django.contrib import admin
from .models import Pendaftar, Kegiatan


admin.site.register(Kegiatan)
admin.site.register(Pendaftar)
