from django.db import models


class Kegiatan(models.Model):
    nama = models.CharField(max_length=100)
    deskripsi = models.TextField()
    tanggal_dibuat = models.DateTimeField(auto_now_add=True)


class Pendaftar(models.Model):
    nama = models.CharField(max_length=70)
    kegiatan = models.ForeignKey(
        Kegiatan, on_delete=models.CASCADE, related_name='pendaftar')
