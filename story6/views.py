from django.shortcuts import render, get_object_or_404, reverse, redirect
from .models import Kegiatan, Pendaftar
from .forms import TambahPendaftarForm, TambahKegiatanForm


def all_kegiatan_view(request):
    kegiatan_all = Kegiatan.objects.all()
    context = {
        "kegiatan_all": kegiatan_all
    }
    return render(request, "story6/kegiatan.html", context)


def add_kegiatan_view(request):
    form = TambahKegiatanForm()
    if request.method == "POST":
        form = TambahKegiatanForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect(reverse("story6:all-kegiatan"))
    context = {
        "kegiatan_form": form
    }
    return render(request, "story6/kegiatan-add.html", context)


def kegiatan_add_pendaftar_post(request, id):
    add_member_form = TambahPendaftarForm(request.POST)
    if add_member_form.is_valid():
        add_member_form.save()
    kegiatan_requested = get_object_or_404(Kegiatan, pk=id)
    if add_member_form.is_valid():
        add_member_form = TambahPendaftarForm(
            initial={"kegiatan": kegiatan_requested})
    context = {
        "kegiatan_requested": kegiatan_requested,
        "add_member_form": add_member_form
    }
    return render(request, "story6/kegiatan-detail.html", context)


def kegiatan_delete_pendaftar(request, id):
    try:
        id_pendaftar = request.POST.get("id")
        Pendaftar.objects.get(pk=id_pendaftar).delete()
    finally:
        kegiatan_requested = get_object_or_404(Kegiatan, pk=id)
        add_member_form = TambahPendaftarForm(
            initial={"kegiatan": kegiatan_requested})
        context = {
            "kegiatan_requested": kegiatan_requested,
            "add_member_form": add_member_form
        }
        return render(request, "story6/kegiatan-detail.html", context)


def kegiatan_detail_view(request, id):
    if request.method == "POST":
        return kegiatan_add_pendaftar_post(request, id)

    kegiatan_requested = get_object_or_404(Kegiatan, pk=id)
    add_member_form = TambahPendaftarForm(
        initial={"kegiatan": kegiatan_requested})
    context = {
        "kegiatan_requested": kegiatan_requested,
        "add_member_form": add_member_form
    }
    return render(request, "story6/kegiatan-detail.html", context)
