from django import forms
from .models import Kegiatan, Pendaftar


class TambahPendaftarForm(forms.ModelForm):
    class Meta:
        model = Pendaftar
        fields = "__all__"
        widgets = {
            "kegiatan": forms.HiddenInput()
        }


class TambahKegiatanForm(forms.ModelForm):
    class Meta:
        model = Kegiatan
        fields = "__all__"
