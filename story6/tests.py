from django.test import TestCase, override_settings
from django.test import Client
from django.urls import resolve
from django.shortcuts import reverse
from .views import all_kegiatan_view, add_kegiatan_view, kegiatan_detail_view
from .models import Kegiatan, Pendaftar


@override_settings(STATICFILES_STORAGE='django.contrib.staticfiles.storage.StaticFilesStorage')
class KegiatanUnitTest(TestCase):
    def setUp(self):
        self.kegiatan_name = 'Testy'
        self.test_kegiatan = Kegiatan.objects.create(
            nama=self.kegiatan_name, deskripsi='Funnn')
        self.pendaftar = Pendaftar.objects.create(
            nama='Haiasfdfsd', kegiatan=self.test_kegiatan)

    def test_kegiatan_all_url_is_exist(self):
        response = Client().get(reverse('story6:all-kegiatan'))
        self.assertEqual(response.status_code, 200)

    def test_kegiatan_all_using_view_func(self):
        found = resolve(reverse('story6:all-kegiatan'))
        self.assertEqual(found.func, all_kegiatan_view)

    def test_kegiatan_add_url_is_exist(self):
        response = Client().get(reverse('story6:add-kegiatan'))
        self.assertEqual(response.status_code, 200)

    def test_kegiatan_add_using_view_func(self):
        found = resolve(reverse('story6:add-kegiatan'))
        self.assertEqual(found.func, add_kegiatan_view)

    def test_kegiatan_detail_url_is_exist(self):
        response = Client().get(reverse('story6:kegiatan-detail',
                                        kwargs={'id': 1}))
        self.assertEqual(response.status_code, 200)

    def test_kegiatan_detail_using_view_func(self):
        found = resolve(reverse('story6:kegiatan-detail',
                                kwargs={'id': 1}))
        self.assertEqual(found.func, kegiatan_detail_view)

    def test_kegiatan_add_post_success_and_render_the_result(self):
        test = 'Haha'
        response_post = Client().post(
            reverse('story6:add-kegiatan'), {'nama': test, 'deskripsi': 'Todassad'})
        self.assertEqual(response_post.status_code, 302)

        html_response = Client().get(reverse('story6:all-kegiatan')).content.decode('utf8')
        self.assertIn(test, html_response)

    def test_pendaftar_add_post_success_and_render_the_result(self):
        response_post = Client().post(
            reverse('story6:kegiatan-detail',
                    kwargs={'id': 1}), {'nama': 'WH', 'kegiatan': self.test_kegiatan.id})
        self.assertEqual(response_post.status_code, 200)

        html_response = response_post.content.decode('utf8')
        self.assertIn('WH', html_response)

    def test_kegiatan_pendaftar_delete_post_success_and_render_the_result(self):
        response_post = Client().post(
            reverse('story6:pendaftar-delete',
                    kwargs={'id': 1}), {'id': self.pendaftar.id})
        self.assertEqual(response_post.status_code, 200)

        html_response = response_post.content.decode('utf8')
        self.assertNotIn(self.pendaftar.nama, html_response)

    # def test_kegiatan_pendaftar_delete_post_fail_and_render_the_result(self):
    #     test = 'Bla'
    #     pd = Pendaftar.objects.create(nama='Bla', acara=self.test_kegiatan)
    #     response_post = Client().post(
    #         '/activities/activities/Test/delete', {'id': 2})
    #     self.assertEqual(response_post.status_code, 200)

    #     html_response = response_post.content.decode('utf8')
    #     self.assertIn(test, html_response)
