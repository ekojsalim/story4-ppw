from django.urls import path
from .views import all_kegiatan_view, kegiatan_detail_view, kegiatan_delete_pendaftar, add_kegiatan_view

app_name = "story6"

urlpatterns = [
    path("", all_kegiatan_view, name="all-kegiatan"),
    path("detail/<int:id>", kegiatan_detail_view, name="kegiatan-detail"),
    path("delete/<int:id>", kegiatan_delete_pendaftar, name="pendaftar-delete"),
    path("add", add_kegiatan_view, name="add-kegiatan")
]
