from django.shortcuts import render


def index(req):
    return render(req, "index.html")


def projects(req):
    return render(req, "projects.html")
