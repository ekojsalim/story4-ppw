from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('timey/', include('timey.urls')),
    path('story1/', include('story1.urls')),
    path('story5/', include('story5.urls')),
    path('story6/', include('story6.urls')),
    path('', include('story3.urls'))
]
